################################################################################################
# quarantineScent by cd projekcik RGB - Open Source Beta Release 0.9 for CS University Course  #
#                                                                                              #
# Inspiration from Techwithtim PyGames "Agar IO"                                               #  
# Lead Network Programming, Debugging by UnusedVariable                                        # 
# Game Design, Gameplay Programming, Debugging by Sable                                        #
# Technical Friendly Help by Siara                                                             #
#                                                                                              #
# Documentation's in another file, feel free to develop this game.                             #
# First run the server.py (If you run the server only) and then game.py from terminal.         #
# Don't forget configure server connection                                                     #
#                                                                                              #
################################################################################################

#!/bin/env python
"""
main server script for running  server

can handle multiple/infinite connections on the same
local network
"""
import re
import CONSTS
import socket
from _thread import *
import _pickle as pickle
import time
import random
import math

# setup sockets
S = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
S.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)	

# Set constants
PORT = #INSERT PORT NUMBER

ROUND_TIME = 60 * 5


SERVER_IP = #INSERT SERVER IP NUMBER
# try to connect to server
try:
    S.bind((SERVER_IP, PORT))
except socket.error as e:
    print(str(e))
    print("[SERVER] Server could not start")
    quit()

S.listen()  # listen for connections

print(f"[SERVER] Server Started with local ip {SERVER_IP}")

# dynamic variables
players = {}
potions = []
connections = 0
direction=[1,2,3,4] #1-w lewo 2-góra 3-prawo 4-dół 
_id = 0
start = False
stat_time = 0
game_time = "Starting Soon"
to_restart = 0


fields=[[0 for i in range(CONSTS.SIZE)] for j in range(CONSTS.SIZE)]

#fields=[ [0] * N for _ in range(M)]
chests = []

for i in range (0,CONSTS.CHESTS_AMOUNT):
	x = random.randint(0, CONSTS.SIZE-1)
	y = random.randint(0, CONSTS.SIZE-1)
	fields[x][y]=1
	chests.append((x,y))
	

# FUNCTIONS

def field_type(x,y):
	
	return fields[x][y]


def spawn_potions(potions):		

	while (len(potions)<CONSTS.POTIONS_AMOUNT):
		x = random.randrange(0,CONSTS.SIZE)
		y = random.randrange(0,CONSTS.SIZE)
		if field_type(x,y) == 1:
			if ((x,y)) not in potions:
				potions.append((x,y))
	print ("[GAME] Potions spawned")


		
def level_restart():
	global connections, players, potions, game_time, start, to_restart, chests, fields, host
	if to_restart==0:
		return
	to_restart=0
	
	fields=[[0 for i in range(CONSTS.SIZE)] for j in range(CONSTS.SIZE)]
	chests = []
	for i in range (0,CONSTS.CHESTS_AMOUNT):
		x = random.randint(0, CONSTS.SIZE-1)
		y = random.randint(0, CONSTS.SIZE-1)
		fields[x][y]=1
		chests.append((x,y))
	time.sleep(0.25)
	spawn_potions(potions)
	time.sleep(10)
	if len(players)>0:		#if liczba graczy > 1 then start
		start = True
		start_time = time.time()
		#playedflag=True
		print("[STARTED] Game Started")
	for i in players:
		#print(str(players[i]["name"]))
		players[i]["score"]=0
	winner=""


def get_start_location(players):
	"""
	picks a start location for a player based on other player
	locations. It wiill ensure it does not spawn inside another player

	:param players: dict
	:return: tuple (x,y)
	"""
	while True:
		stop = True
		x = random.randrange(0,CONSTS.SIZE)
		y = random.randrange(0,CONSTS.SIZE)
		if field_type(x,y) == 0:
			stop = False
			break
		if stop:
			break
	return (x,y)


def threaded_client(conn, _id):
	"""
	runs in a new thread for each player connected to the server

	:param con: ip address of connection
	:param _id: int
	:return: None
	"""
	global connections, players, potions, game_time, start, to_restart, chests, fields

	current_id = _id

	# recieve a name from the client
	data = conn.recv(16)
	name = data.decode("utf-8")
	print("[LOG]", name, "connected to the server.")

	# Setup properties for each new player
	x, y = get_start_location(players)
	players[current_id] = {"x":x, "y":y,"score":0,"name":name,"direction":1}  # x, y color, score, name, direction
	# pickle data and send initial info to clients
	conn.send(str.encode(str(current_id)))

	# server will recieve basic commands from client
	# it will send back all of the other clients info
	'''
	commands start with:
	move
	jump
	get
	id - returns id of client
	'''
	winner=""
	while True:

		if start:
			game_time = round(time.time()-start_time)
			# if the game time passes the round time the game will stop
			sort_players = list(reversed(sorted(players, key=lambda x: players[x]["score"])))
			if game_time >= ROUND_TIME:
				start = False
				to_restart=1
				print("[GAME] Time is up. Noone won")
			if players[ sort_players[0] ]["score"] >= 3:
				start = False
				winner=players[ sort_players[0] ]["name"]
				print("[GAME] The winner of this round is " + str(winner))
				to_restart=1
		
		try:
			# Recieve data from client
			data = conn.recv(32)

			if not data:
				break

			data = data.decode("utf-8")
			#print("[DATA] Recieved", data, "from client id:", current_id)

			# look for specific commands from recieved data
			if data.split(" ")[0] == "move":					#dodac kierunek patrzenia
				split_data = data.split(" ")
				x = int(split_data[1])
				y = int(split_data[2])
				direction = int(split_data[3])
				score = int(split_data[4])
				players[current_id]["x"] = x
				players[current_id]["y"] = y
				players[current_id]["direction"] = direction
				players[current_id]["score"] = score
			
				send_data = pickle.dumps((potions,players,game_time,chests)) 

			elif data.split(" ")[0] == "id":
				send_data = str.encode(str(current_id))  # if user requests id then send it
			elif data.split(" ")[0] == "potpot":
				
				xtoremove = data.split(" ")[1]
				x=int(re.search(r'\d+', xtoremove).group())
				ytoremove = data.split(" ")[2]
				y=int(re.search(r'\d+', ytoremove).group())
				
				potiontoremove= (x,y)
				
				if potiontoremove in potions:
					#print("jest w potionach")
					potions.remove(potiontoremove)
				send_data = pickle.dumps((potions,players, game_time,chests))
			elif data.split(" ")[0] == "jump":
				send_data = pickle.dumps((potions,players, game_time,chests))
			else:
				# any other command just send back list of players
				send_data = pickle.dumps((potions,players, game_time, chests))

			# send data back to clients
			conn.send(send_data)

		except Exception as e:
			print(e)
			break  # if an exception has been reached disconnect client

		time.sleep(0.001)

	# When user disconnects
	print("[DISCONNECT] Name:", name, ", Client Id:", current_id, "disconnected")

	connections -= 1
	del players[current_id]  # remove client information from players list
	conn.close()  # close connection


# MAINLOOP

# setup level with potions
spawn_potions(potions)

print("[GAME] Setting up level")
print("[SERVER] Waiting for connections")


print("Potions "+ str(sorted(potions)))
print("Chests "+str(sorted(chests)))

while True:								
	host, addr = S.accept()
	print("[CONNECTION] Connected to:", addr)
	print("liczba graczy "+ str(len(players)))
	# start game when a client on the server computer connects
	if len(players)>0:		#if liczba graczy > 1 then start
		start = True
		start_time = time.time()
	
		print("[STARTED] Game Started")


	connections += 1
	start_new_thread(threaded_client,(host,_id))
	_id += 1

# when program ends
print("[SERVER] Server offline")