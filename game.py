################################################################################################
# quarantineScent by cd projekcik RGB - Open Source Beta Release 0.9 for CS University Course  #
#                                                                                              #
# Inspiration from Techwithtim PyGames "Agar IO"                                               #  
# Lead Network Programming, Debugging by UnusedVariable                                        # 
# Game Design, Gameplay Programming, Debugging by Sable                                        #
# Technical Friendly Help by Siara                                                             #
#                                                                                              #
# Documentation's in another file, feel free to develop this game.                             #
# First run the server.py (If you run the server only) and then game.py from terminal.         #
# Don't forget configure server connection                                                     #
#                                                                                              #
################################################################################################

#!/bin/env python
# small network game that has differnt blobs
# moving around the screen
import CONSTS
import contextlib
with contextlib.redirect_stdout(None): 
    import pygame
from client import Network
import random
import os
pygame.font.init()

vel = 1

NAME_FONT = pygame.font.SysFont("comicsans", 20)
TIME_FONT = pygame.font.SysFont("comicsans", 30)
SCORE_FONT = pygame.font.SysFont("comicsans", 26)
WINNER_FONT = pygame.font.SysFont("comicsans", 80)


# Dynamic Variables
players = {}
potions = []
fields=[[0 for i in range(CONSTS.SIZE)] for j in range(CONSTS.SIZE)]

chests = []
# FUNCTIONS

#GRAPHICS

floor= pygame.image.load('pics/floor.png')
chest = pygame.image.load('pics/chestt.png')
character = [pygame.image.load('pics/char_one.png'), pygame.image.load('pics/char_two.png'), pygame.image.load('pics/char_three.png'), pygame.image.load('pics/char_four.png')]


def field_type(x,y):
	return fields[x][y]

def convert_time(t):
	"""
	converts a time given in seconds to a time in
	minutes

	:param t: int
	:return: string
	"""
	if type(t) == str:
		return t

	if int(t) < 60:
		return str(t) + "s"
	else:
		minutes = str(t // 60)
		seconds = str(t % 60)

		if int(seconds) < 10:
			seconds = "0" + seconds

		return minutes + ":" + seconds

def check_potion(player, potions):
	if (player["direction"] == 1):
		if (player["x"]-1, player["y"]) in potions:
			player["score"] = player["score"]+1
		return ((player["x"]-1, player["y"]))

	if (player["direction"] == 2):
		if (player["x"], player["y"]-1) in potions:
			player["score"] = player["score"]+1
		return ((player["x"], player["y"]-1))

	if (player["direction"] == 3):
		if (player["x"]+1, player["y"]) in potions:
			player["score"] = player["score"]+1
		return ((player["x"]+1, player["y"]))

	if (player["direction"] == 4):
		if (player["x"], player["y"]+1) in potions:
			player["score"] = player["score"]+1
		return ((player["x"], player["y"]+1))



def redraw_window(players, game_time, score,fields):
	"""
	draws each frame
	:return: None
	"""
#	WIN.fill((255,255,255)) # fill screen white, to clear old frames
	for i in range(0,CONSTS.SIZE):
		for j in range(0,CONSTS.SIZE):
			#print("i="+str(i)+"  j="+str(j))
			if fields[i][j] == 0:
				WIN.blit(floor,(i*40,j*40))
			else:
				WIN.blit(chest,(i*40,j*40))

	# draw each player in the list
	for player in sorted(players, key=lambda x: players[x]["score"]):
		p = players[player]
		# render and draw name for each player
		if (p["direction"]==1):
			WIN.blit(character[0],(p["x"]*40,p["y"]*40))
		elif (p["direction"]==2):
			WIN.blit(character[1],(p["x"]*40,p["y"]*40))
		elif (p["direction"]==3):
			WIN.blit(character[2],(p["x"]*40,p["y"]*40))
		else:
			WIN.blit(character[3],(p["x"]*40,p["y"]*40))
			
		text = NAME_FONT.render(p["name"], 1, (0,0,0))
		WIN.blit(text, (p["x"]*40 , p["y"]*40+40)) 
	# draw scoreboard/
	sort_players = list(reversed(sorted(players, key=lambda x: players[x]["score"])))
	title = TIME_FONT.render("Scoreboard", 1, (0,0,0))
	start_y = 25
	x = CONSTS.W - title.get_width() - 10
	WIN.blit(title, (x, 5))
	if players[ sort_players[0] ]["score"] >= 3:
		text= WINNER_FONT.render("The winner is " + str(players[ sort_players[0] ]["name"]),1, (0,0,0))
		text_center = text.get_rect(center=(CONSTS.W/2, CONSTS.H/2))
		WIN.blit(text, text_center)


	ran = min(len(players), 2)
	for count, i in enumerate(sort_players[:ran]):
		text = SCORE_FONT.render(str(count+1) + ". " + str(players[i]["name"]) + "   " + str(players[i]["score"]), 1, (0,0,0))
		WIN.blit(text, (x, start_y + count * 20))

	# draw time
	text = TIME_FONT.render("Time: " + convert_time(game_time), 1, (0,0,0))
	WIN.blit(text,(10,10))
	# draw score
	text = TIME_FONT.render("Score: " + str(round(score)),1,(0,0,0))
	WIN.blit(text,(10,15 + text.get_height()))

def main(name):
	"""
	function for running the game,
	includes the main loop of the game

	:param players: a list of dicts represting a player
	:return: None
	"""
	global players

	# start by connecting to the network
	server = Network()
	current_id = server.connect(name) #server.connect("sable") will have player id in 8 bytes i guess
	potions, players, game_time, chests = server.send("get") #pobieramy informacje z serwera.
	#potions, players, game_time = server.send("get") #pobieramy informacje z serwera.

	for (x,y) in chests:
		fields[x][y]=1
	# setup the clock, limit to 30fps
	clock = pygame.time.Clock()

	run = True
	while run:
		clock.tick(30) # 30 fps max
		player = players[current_id] #nasza postać


		# get key presses
		keys = pygame.key.get_pressed()

		data = ""
	
		#32x18 maze
		if keys[pygame.K_LEFT] or keys[pygame.K_a]:
			if (field_type(player["x"]-vel, player["y"])==1 or player["x"]==0):
				player["direction"] = 1
			else:
				player["x"] = player["x"]-vel
				player["direction"]=1

		if keys[pygame.K_RIGHT] or keys[pygame.K_d]:
			#print("SIZE="+str(CONSTS.SIZE))
			#print(player["x"])
			if (player["x"]==CONSTS.SIZE-1 or field_type(player["x"]+vel, player["y"])==1):
				player["direction"]= 3
			else:
				player["x"] = player["x"]+vel
				player["direction"]=3

		if keys[pygame.K_UP] or keys[pygame.K_w]:
			if (field_type(player["x"], player["y"]-vel)==1 or player["y"]==0):
				player["direction"]=2
			else:
				player["y"] = player["y"]-vel
				player["direction"]=2

		if keys[pygame.K_DOWN] or keys[pygame.K_s]:
			if (player["y"] == CONSTS.SIZE-1 or fields[player["x"]][player["y"]+vel]==1):
				player["direction"] = 4
			else:
				player["y"] = player["y"]+vel
				player["direction"]=4
		if keys[pygame.K_SPACE]:
				potiontoremove=check_potion(player, potions)
				#print(str(potiontoremove))
				data="potpot " + str(potiontoremove)
				potions, players, game_time, chests = server.send(data)
				
		
		data = "move " + str(player["x"]) + " " + str(player["y"]) + " " + str(player["direction"]) + " " + str(player["score"])
	
		potions, players, game_time, chests = server.send(data)
		#print("Potions "+ str(sorted(potions)))
		#print("Chests "+str(sorted(chests)))

		

		#wychodzenie z gry
		for event in pygame.event.get():
			# if user hits red x button close window
			if event.type == pygame.QUIT:
				run = False

			if event.type == pygame.KEYDOWN:
				# if user hits a escape key close program
				if event.key == pygame.K_ESCAPE:
					run = False


		# redraw window then update the frame
		redraw_window(players, game_time, player["score"],fields)
		pygame.display.update()


	server.disconnect()
	pygame.quit()
	quit()


# get users name
while True:
 	name = input("Please enter your name: ")
 	if  0 < len(name) < 20:
 		break
 	else:
 		print("Error, this name is not allowed (must be between 1 and 19 characters [inclusive])")

# make window start in top left hand corner
os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (0,30) # ???

# setup pygame window
WIN = pygame.display.set_mode((CONSTS.W,CONSTS.H)) #Powinny być podane przez różnice rozdzielczości, depth parametr do sprawdzenia przy zrobieniu tła
pygame.display.set_caption("quarantineScent") #nazwa okienka

# start game
main(name)