################################################################################################
# quarantineScent by cd projekcik RGB - Open Source Beta Release 0.9 for CS University Course  #
#                                                                                              #
# Inspiration from Techwithtim PyGames "Agar IO"                                               #  
# Lead Network Programming, Debugging by UnusedVariable                                        # 
# Game Design, Gameplay Programming, Debugging by Sable                                        #
# Technical Friendly Help by Siara                                                             #
#                                                                                              #
# Documentation's in another file, feel free to develop this game.                             #
# First run the server.py (If you run the server only) and then game.py from terminal.         #
# Don't forget configure server connection                                                     #
#                                                                                              #
################################################################################################
SIZE=20
W = SIZE*40
H = SIZE*40
CHESTS_AMOUNT=15
POTIONS_AMOUNT=5