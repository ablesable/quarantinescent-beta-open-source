################################################################################################
# quarantineScent by cd projekcik RGB - Open Source Beta Release 0.9 for CS University Course  #
#                                                                                              #
# Inspiration from Techwithtim PyGames "Agar IO"                                               #  
# Lead Network Programming, Debugging by UnusedVariable                                        # 
# Game Design, Gameplay Programming, Debugging by Sable                                        #
# Technical Friendly Help by Siara                                                             #
#                                                                                              #
# Documentation's in another file, feel free to develop this game.                             #
# First run the server.py and then game.py from terminal.                                      #
# Don't forget configure server connection                                                     #
#                                                                                              #
################################################################################################

import socket
import _pickle as pickle


class Network:
    """
    class to connect, send and recieve information from the server

    need to hardcode the host attirbute to be the server's ip
    """
    def __init__(self):
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #self.client.settimeout(10.0)
        self.host = #INSERT CLIENT IP NUMBER
        self.port = #INSERT PORT OF CLIENT NUMBER
        self.addr = (self.host, self.port)

    def connect(self, name):
        """
        connects to server and returns the id of the client that connected
        :param name: str
        :return: int reprsenting id
        """
        self.client.connect(self.addr)
        self.client.send(str.encode(name))
        val = self.client.recv(8) #returns players id in 8 bytes i guess
        return int(val.decode()) # can be int because will be an int id

    def disconnect(self):
        """
        disconnects from the server
        :return: None
        """
        self.client.close()

    def send(self, data, pick=False):
        """
        sends information to the server

        :param data: str
        :param pick: boolean if should pickle or not
        :return: str
        """
        try:
            if pick:
                self.client.send(pickle.dumps(data))
            else:
                self.client.send(str.encode(data))
            reply = self.client.recv(2048*4)
            try:
                reply = pickle.loads(reply)
            except Exception as e:
                print(e)

            return reply
        except socket.error as e:
            print(e)



